//
//  Graph.cpp
//  CallgraphVisualizer
//
//  Created by robin on 3/16/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#include "Graph.h"
#include "Tree.h"
#include "GraphNode.h"
#include "util.h"

#include <unordered_map>
#include <ogdf/layered/SugiyamaLayout.h>
#include <ogdf/layered/OptimalRanking.h>
#include <ogdf/layered/MedianHeuristic.h>
#include <ogdf/layered/OptimalHierarchyLayout.h>
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/cluster/ClusterGraph.h>
#include <cassert>

using namespace boost;
using std::cout;
using std::endl;
using namespace draw_utility;

MyGraph::MyGraph(const std::string & path) :
GA(OG, GraphAttributes::nodeLevel | GraphAttributes::nodeId |
GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics |
GraphAttributes::nodeLabel | GraphAttributes::nodeColor |
GraphAttributes::edgeColor | GraphAttributes::edgeStyle |
GraphAttributes::nodeStyle | GraphAttributes::nodeTemplate) {
    level_to_expand=2;
    utility::loadDiGraph(path, g, dp, uid2vertex);
    history.push(rootUid);
   
}
void MyGraph::constructGraph(int uid)
{
    // delete old graph
    if(graph != NULL)
       delete graph;
    graph = makeNode(uid, getWeight(uid), getLabel(uid),g[uid2vertex[uid]].position);
     
    std::vector<NodePtr> newQueue;
    newQueue.push_back(graph);
    int level = this->level_to_expand;
    // expand a level
    for (int i = 0; i < level - 1; ++i) {
        std::vector<NodePtr> oldQueue;
        oldQueue.swap(newQueue);
        for (NodePtr p : oldQueue) {
            p->setVisible(true);
            std::vector<NodePtr> neighbors = getOutNeighbors(p->getUid());
            newQueue.insert(newQueue.end(), neighbors.begin(), neighbors.end());
            p->addChildren(neighbors);
            for(NodePtr c:p->getChildren()){	
                c->setVisible(true);	
		}         
        }
    }

  // upward display
   std::vector<NodePtr> newQueue2;
    newQueue2.push_back(graph);
    // expand a level
    for (int i = 0; i < level - 1; ++i) {
        std::vector<NodePtr> oldQueue;
        oldQueue.swap(newQueue2);
        for (NodePtr p : oldQueue) {
            p->setVisible(true);
            std::vector<NodePtr> neighbors = getInNeighbors(p->getUid());
            int length=neighbors.size();
            printf("%d has %d parents\n",p->getUid(),length);
            newQueue2.insert(newQueue2.end(), neighbors.begin(), neighbors.end());
            p->addParent(neighbors);
            for(NodePtr pp : p->getParent()){                	
                pp->setVisible(true);	
                //if(pp->node_visible())
                //  printf("can you see my %d parent %d?\n",p->getUid(),pp->getUid());
		}         
        }
    }
    
}
void MyGraph::Render()
{
       graph->Render();  
}

void MyGraph::Pick()
{
    GLubyte color[3];
    graph->Pick();
}
bool MyGraph::onPick(int uid, const MouseAction& mouseAction)
{
    std:: cout << g[uid2vertex[uid]].id << std::endl;
    
    if (mouseAction.action() == MouseActionType::LEFT_CLICK) {
        // check if uid is valid
        if (history.top() == uid || uid2vertex.count(uid) == 0 ||uid>OG.numberOfNodes())
            return false;
        else {
            history.push(uid);
            constructGraph(uid);
            return true;
        }
    }
    else
        return false;
  
    return true;
}

void MyGraph::getHierarchicalLayout(int width, int height) {
    using namespace ogdf;

    OG.clear();
    GA.clearAllBends();
    uid2ognode.clear();

    // convert boost graph to ogdf graph

    BGL_FORALL_VERTICES(v, g, DiGraph) {
        ogdf::node onode = OG.newNode();
        GA.idNode(onode) = g[v].uid;
        GA.labelNode(onode) = g[v].label.c_str();
        uid2ognode.insert(std::make_pair(g[v].uid, onode));
    }

    BGL_FORALL_EDGES(e, g, DiGraph) {
        int src = g[source(e, g)].uid;
        int dst = g[target(e, g)].uid;
        ogdf::edge oedge = OG.newEdge(uid2ognode[src], uid2ognode[dst]);
    }

    printf("#v:%d, #e: %d\n", OG.numberOfNodes(), OG.numberOfEdges());

    SugiyamaLayout SL;
    SL.setRanking(new OptimalRanking);
    SL.setCrossMin(new MedianHeuristic);

    OptimalHierarchyLayout *ohl = new OptimalHierarchyLayout;
    ohl->layerDistance(50.0);
    ohl->nodeDistance(60.0);
    ohl->weightBalancing(1.0);
    SL.setLayout(ohl);

    NodeArray <int> rank;
    SL.call(GA, rank); //to store the ranking in this array
//    printf("total number of layers: %d\n", SL.numberOfLevels());
//    printf("max layer size: %d\n", SL.maxLevelSize());
    // assign the layout back
    SList<node> nlist;
    OG.allNodes(nlist);
    DRect rect = GA.boundingBox();
    for (auto N : nlist) {
        int uid = GA.idNode(N);
        g[uid2vertex[uid]].position[0] = GA.x(N);
        g[uid2vertex[uid]].position[1] = GA.y(N);
        g[uid2vertex[uid]].level = rank[N]; //store ranking in vertexes
    }
    rootUid = utility::getRoot(g);
    constructGraph(rootUid);

}

Vertex& MyGraph::getVertexByUid(int uid) {
    return g[uid2vertex[uid]];
}

int MyGraph::getRoot() {
    return utility::getRoot(g);
}

MyGraph::~MyGraph()
{
    if ( graph!= NULL)
        delete this->graph;
}

std::string MyGraph::getLabel(int uid)
{
    return g[uid2vertex[uid]].label;
}

int MyGraph::getOutDegree(int uid)
{
    return boost::out_degree(uid2vertex[uid], g);
}

double MyGraph::getWeight(int uid)
{
    return 1 + sqrt(sqrt(getOutDegree(uid)));
}

std::vector<NodePtr> MyGraph::getOutNeighbors(int uid)
{
    DiGraph::out_edge_iterator outedgeIt, outedgeEnd;
    tie(outedgeIt, outedgeEnd) = boost::out_edges(uid2vertex[uid], g);

    std::vector<int> neighbor_ids;
    for (; outedgeIt != outedgeEnd; ++outedgeIt) {
        Vertex v = g[boost::target(*outedgeIt, g)];
        neighbor_ids.push_back(v.uid);
         printf("my %d child is %d\n",uid,v.uid);
    }

    std::vector<NodePtr> ans;
    for (int id : neighbor_ids) {
        ans.push_back(makeNode(id, getWeight(id), getLabel(id),g[uid2vertex[id]].position));
    }
    return ans;
}

std::vector<NodePtr> MyGraph::getInNeighbors(int uid)
{
    DiGraph::in_edge_iterator inedgeIt, inedgeEnd;
    tie(inedgeIt, inedgeEnd) = boost::in_edges(uid2vertex[uid], g);

    std::vector<int> neighbor_ids;
    for (; inedgeIt != inedgeEnd; ++inedgeIt) {
        Vertex v = g[boost::source(*inedgeIt, g)];
        neighbor_ids.push_back(v.uid);
        printf("my %d parent is %d\n",uid,v.uid);
    }

    std::vector<NodePtr> ans;
    for (int id : neighbor_ids) {
        ans.push_back(makeNode(id, getWeight(id), getLabel(id),g[uid2vertex[id]].position));
    }
    return ans;
}

void MyGraph::undo()
{
    history.pop();
    if (history.empty())
        history.push(rootUid);
}

void MyGraph::setLevelToExpand(int level)
{
    this->level_to_expand = level;
}


void MyGraph::reconstruct()
{
   constructGraph(history.top());
}

void MyGraph::constructMain()
{
   constructGraph(rootUid);
}


