#ifndef GRAPHNODE_H
#define GRAPHNODE_H


#include <vector>
#include "vec2.h"
#include <memory>
#include <algorithm>
#include <string>
#include "rect.h"
#include "draw_utility.h"
#include "util.h"
#include "mouse.h"
class Node;
typedef Node* NodePtr;
class Node
{
    int uid; // unique for the function
    int weight; // weight used for decomposition
    vec2 position; //position for draw
    std::string label;
    bool _visible; // whether the node should be drawn
    std::vector<NodePtr> children;
    std::vector<NodePtr> parent;

public:
    virtual ~Node();

    int getUid() const;
    int getWeight() const;
    std::vector<NodePtr> getChildren() const;
    std::vector<NodePtr> getParent() const;
    void setWeight(int weight);
    std::string getLabel() const;
    void setVisible(bool flag){_visible=flag;}
    void setLabel(std::string label);
    void setPosition(vec2 position);
    void addChild(const NodePtr& child);
    void addChildren(const std::vector<NodePtr>& rhs);
    void addParent(const std::vector<NodePtr>& rhs);
    void Render();
    void Pick();
    bool node_visible();
    friend NodePtr makeNode(int uid, double weight, std::string label,vec2 position);

private:
    Node(int uid, double weight);
   
    
//    bool label_visible();

};

NodePtr makeNode(int uid, double weight, std::string label, vec2 position);

#endif
