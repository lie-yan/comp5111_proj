/* 
 * File:   Tree.h
 * Author: robin
 *
 * Created on April 13, 2014, 1:23 PM
 */

#ifndef TREE_H
#define	TREE_H

#include <vector>
#include <memory>
#include <algorithm>
#include <string>
#include "rect.h"
#include "draw_utility.h"
#include "util.h"
#include "mouse.h"

class TreeNode;
typedef TreeNode* TreeNodePtr;
typedef TreeNode Tree;

class TreeNode
{
    int uid; // unique for the function
    int weight; // weight used for decomposition
    std::string label;

    rect box; // rectangle for the node
    bool _visible; // whether the node should be drawn

    std::vector<TreeNodePtr> children;

public:
    virtual ~TreeNode();

    int getUid() const;
    int getWeight() const;
    void setWeight(int weight);
    std::string getLabel() const;
    void setLabel(std::string label);
    rect getBox() const;
    void setBox(const rect& box);

    void addChild(const TreeNodePtr& child);
    void addChildren(const std::vector<TreeNodePtr>& rhs);

    void recursive_nested_squalify(const rect& box);
    void printLayout();

    void Render();
    void Pick();

    friend TreeNodePtr makeTreeNode(int uid, double weight,
                                    std::string label);

private:
    TreeNode(int uid, double weight);

    bool fat();
    bool node_visible();
    bool label_visible();

    void squarify(const rect& box);
    void squarify(int idxCurrNode, std::vector<TreeNodePtr>& row,
                  const rect& box, int sumWeightUnlaid);
    rect layoutRow(const std::vector<TreeNodePtr>& row,
                   const rect& box, int sumWeightUnlaid);
    void layoutRowFill(const std::vector<TreeNodePtr>& row,
                       const rect& box, int unlaidWeigthInBox);
};


TreeNodePtr makeTreeNode(int uid, double weight, std::string label);

#endif	/* TREE_H */

