/* 
 * File:   TreeMapController.cpp
 * Author: robin
 * 
 * Created on April 15, 2014, 4:02 PM
 */

#include "TreeMapController.h"

#include <cassert>


using namespace std;

TreeMapController::TreeMapController(const std::string& path)
: tree(nullptr)
{
    utility::loadDiGraph(path, g, dp, uid2vertex);
    rootUid = utility::getRoot(g);
    history.push(rootUid);

    constructTree(rootUid);
}

TreeMapController::~TreeMapController()
{
    if (tree != NULL)
        delete this->tree;
}

void TreeMapController::constructTree(int uid)
{
    // delete old tree
    if (tree != NULL)
        delete tree;

    tree = makeTreeNode(uid, getWeight(uid), getLabel(uid));
    std::vector<TreeNodePtr> newQueue;
    newQueue.push_back(tree);

    int level = std::max(this->level_to_expand, 2);
    // expand a level
    for (int i = 0; i < level - 1; ++i) {
        std::vector<TreeNodePtr> oldQueue;
        oldQueue.swap(newQueue);
        for (TreeNodePtr p : oldQueue) {
            std::vector<TreeNodePtr> neighbors = getOutNeighbors(p->getUid());
            newQueue.insert(newQueue.end(), neighbors.begin(), neighbors.end());
            p->addChildren(neighbors);
        }
    }
}

std::string TreeMapController::getLabel(int uid)
{
    return g[uid2vertex[uid]].label;
}

int TreeMapController::getOutDegree(int uid)
{
    return boost::out_degree(uid2vertex[uid], g);
}

double TreeMapController::getWeight(int uid)
{
    return 1 + sqrt(sqrt(getOutDegree(uid)));
}


std::vector<TreeNodePtr> TreeMapController::getOutNeighbors(int uid)
{
    DiGraph::out_edge_iterator outedgeIt, outedgeEnd;
    tie(outedgeIt, outedgeEnd) = boost::out_edges(uid2vertex[uid], g);

    std::vector<int> neighbor_ids;
    for (; outedgeIt != outedgeEnd; ++outedgeIt) {
        Vertex v = g[boost::target(*outedgeIt, g)];
        neighbor_ids.push_back(v.uid);
    }

    std::vector<TreeNodePtr> ans;
    for (int id : neighbor_ids) {
        ans.push_back(makeTreeNode(id, getWeight(id),
                                   getLabel(id)));
    }
    return ans;
}

void TreeMapController::Render()
{
    assert(tree != nullptr);
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    
    assert(0 < viewport[2]);
    assert(0 < viewport[3]);
    tree->setBox(rect(0, 0, viewport[2], viewport[3]));
    tree->recursive_nested_squalify(tree->getBox());
    tree->Render();
}

void TreeMapController::Pick()
{
    assert(tree != nullptr);
    tree->Pick();
}

bool TreeMapController::onPick(int uid, const MouseAction& mouseAction)
{
    std:: cout << g[uid2vertex[uid]].id << std::endl;
    
    if (mouseAction.action() == MouseActionType::LEFT_CLICK) {
        // check if uid is valid
        if (history.top() == uid || uid2vertex.count(uid) == 0)
            return false;
        else {
            history.push(uid);
            constructTree(uid);
            return true;
        }
    }
    else
        return false;
}

void TreeMapController::undo()
{
    history.pop();
    if (history.empty())
        history.push(rootUid);
}

void TreeMapController::setLevelToExpand(int level)
{
    this->level_to_expand = level;
}


void TreeMapController::reconstruct()
{
    constructTree(history.top());
}

