//
//  draw_utility.cpp
//  callgraph
//
//  Created by robin on 3/21/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#include "draw_utility.h"
#include "rect.h"

namespace draw_utility
{
    using fms = FontManagerSingleton;

    void drawLabelledNode(vec2 position, const std::string& label)
    {
        drawNode(position);
        vec2 plabel;
        int length=label.length();
 
        plabel[0]=position[0]-3.31*length;
        plabel[1]=position[1]+12;
     //    printf("original position (%f,%f), label position (%f,%f)\n",position[0],position[1],plabel[0],plabel[1]);
        drawLabel(plabel, label);
    }

    void drawNode(vec2 position)
    {
        glPushMatrix();
        glTranslatef(position[0], position[1], 0);
        drawSquare(12);
        glPopMatrix();
    }

    void drawLabel(vec2 position, const std::string& label)
    {
        glPushMatrix();
        glTranslatef(position[0], position[1], 0);
        drawText(label.c_str());
        glPopMatrix();
    }

    void drawArrowedEdge(vec2 from, vec2 to)
    {
        drawSegment(from, to);
        drawArrow(from, to);
    }

    void drawText(const char * str)
    {
        glPushMatrix();

        int lineHeight = fms::getInstance()->LineHeight();
        glRasterPos2f(0.0f, 0.0f + lineHeight); // this is important
        fms::getInstance()->drawText(str);

        glPopMatrix();
    }

    void drawRectangle(vec2 lt, vec2 br)
    {
        glPushMatrix();

        glBegin(GL_LINE_LOOP);
        glVertex2f(lt[0], lt[1]);
        glVertex2f(br[0], lt[1]);
        glVertex2f(br[0], br[1]);
        glVertex2f(lt[0], br[1]);
        glVertex2f(lt[0], lt[1]);

        glEnd();

        glPopMatrix();
    }

    void drawPickRectangle(vec2 lt, vec2 br)
    {
        glPushMatrix();

        glBegin(GL_QUADS);
        glVertex2f(lt[0], lt[1]);
        glVertex2f(lt[0], br[1]);
        glVertex2f(br[0], br[1]);
        glVertex2f(br[0], lt[1]);
        glEnd();
        glPopMatrix();
    }

    void drawSquare(float side)
    {
        glPushMatrix();

        glTranslatef(-side / 2, -side / 2, 0);
        glBegin(GL_QUADS);
        glVertex2f(0, 0);
        glVertex2f(0, side);
        glVertex2f(side, side);
        glVertex2f(side, 0);
        glEnd();

        glPopMatrix();
    }

    void drawSegment(const vec2& a, const vec2& b)
    {
        glBegin(GL_LINES);
        glVertex2f(a[0], a[1]);
        glVertex2f(b[0], b[1]);
        glEnd();
    }

    void drawArrow(const vec2& from, const vec2& to)
    {
        vec2 direct = (to - from).unit_vector();
        vec2 perp = direct.perp_vector();

        int disp = 10, height = 13, half_width = 4;
        glBegin(GL_TRIANGLES);
        glVertex2f(to[0] - disp * direct[0], to[1] - disp * direct[1]);
        glVertex2f(to[0] - (height + disp) * direct[0] - half_width * perp[0],
                   to[1] - (height + disp) * direct[1] - half_width * perp[1]);
        glVertex2f(to[0] - (height + disp) * direct[0] + half_width * perp[0],
                   to[1] - (height + disp) * direct[1] + half_width * perp[1]);
        glEnd();
    }

    double lineHeight()
    {
        return fms::getInstance()->LineHeight();
    }

    rect BBox(const std::string& str)
    {
        FTBBox box =  fms::getInstance()->BBox(str.c_str());
        double width = fabs(box.Upper().X() - box.Lower().X());
        double height = fabs(box.Upper().Y() - box.Lower().Y());  
        
        assert(width >= 0);
        assert(height >= 0);
        return rect(0,0, width, height);
    }

};
