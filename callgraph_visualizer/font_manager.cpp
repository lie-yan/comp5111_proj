//
//  font_manager.cpp
//  callgraph
//
//  Created by robin on 3/21/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#include "font_manager.h"

FontManager * FontManagerSingleton::pInstance = NULL;

void FontManager::setFont(const char * font_path, int size) {
    if (font != NULL) {
        delete font;
        font = NULL;
    }

    font = new FTGLPixmapFont(font_path);
    if (font->Error()) {
        fprintf(stderr, "font '%s' not found\n", font_path);
        exit(-1);
    }
    font->FaceSize(size);
}

void FontManager::drawText(const char* str) {
    font->Render(str);
}

float FontManager::LineHeight() {
    return font->LineHeight();
}

FTBBox FontManager::BBox(const char * str) {
    return font->BBox(str);
}

FontManager * FontManagerSingleton::getInstance() {
    if (pInstance == NULL) {
        pInstance = new FontManager();
    }
    return pInstance;
}