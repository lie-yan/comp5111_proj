//
//  font_manager.h
//  callgraph
//
//  Created by robin on 3/21/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#ifndef __callgraph__font_manager__
#define __callgraph__font_manager__

#include <iostream>

#include <FTGL/ftgl.h>

class FontManager
{
    FTGLPixmapFont * font = NULL;
public:

    ~FontManager()
    {
        delete font;
    }

    void setFont(const char * font_path, int size);
    void drawText(const char* str);
    float LineHeight();
    FTBBox BBox(const char * str);

    friend class FontManagerSingleton;

private:

    FontManager()
    {
    }
};

class FontManagerSingleton
{
    static FontManager * pInstance;
public:
    static FontManager * getInstance();
};

#endif /* defined(__callgraph__font_manager__) */
