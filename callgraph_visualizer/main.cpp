//
//  main.cpp
//  CallgraphVisualizer
//
//  Created by robin on 3/16/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#include <cstdlib>
#include <cstring>

#include <string>
#include <iostream>
#include <memory>
#include <utility>

#include "Graph.h"
#include "font_manager.h"
#include "util.h"
#include "mouse.h"
#include "rect.h"
#include "Tree.h"
#include "TreeMapController.h"

enum MENU_ITEM
{
    MENU_EXIT = 33,
    MENU_HIER_GRAPH,
    MENU_TREE_MAP,
};

enum class EnumWorkMode
{
    HIER_GRAPH,
    TREE_MAP
};

std::unique_ptr<MyGraph> g_graph(nullptr);
std::unique_ptr<TreeMapController> g_treemap(nullptr);
EnumWorkMode g_working_mode = EnumWorkMode::TREE_MAP;

GLint g_viewportWidth = 800, g_viewportHeight = 600;
MouseState g_mouseState;
MouseAction g_mouseAction;


void InitGL();
void InitMenu();
void MenuCallback(int value);
void ReshapeFunc(int width, int height);
void DisplayFunc();

void KeyboardFunc(unsigned char ch, int x, int y);
void MouseFunc(int button, int state, int x, int y);
void MotionFunc(int x, int y);
void PassiveMotionFunc(int x, int y);

void RenderGLScene();
void DrawPickScenes();
bool PickOnMouseChange(const MouseAction& mouseAction);
bool readUidAt(int x, int y, int& uid);

/**
 * init openGL environment
 **/
void InitGL()
{
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Callgraph Visualizer");
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glEnable(GL_MULTISAMPLE);
    glPolygonOffset(1.0, 1.0);
    glDepthFunc(GL_LEQUAL);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_FOG);
    glDisable(GL_LIGHTING);

    glutReshapeFunc(ReshapeFunc);
    glutDisplayFunc(DisplayFunc);
    glutKeyboardFunc(KeyboardFunc);
    glutMouseFunc(MouseFunc);
    glutMotionFunc(MotionFunc);
    glutPassiveMotionFunc(PassiveMotionFunc);
}

/**
 *  init right-click menu
 **/
void InitMenu()
{
    glutCreateMenu(MenuCallback);
    glutAddMenuEntry("Hierarchical graph", MENU_HIER_GRAPH);
    glutAddMenuEntry("Treemap", MENU_TREE_MAP);
    glutAddMenuEntry("Exit", MENU_EXIT);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

/**
 *  GLUT menu callback function
 **/
void MenuCallback(int value)
{
    switch (value) {
    case MENU_EXIT: exit(0);
        break;
    case MENU_HIER_GRAPH:
        g_working_mode = EnumWorkMode::HIER_GRAPH;
        break;

    case MENU_TREE_MAP:
        g_working_mode = EnumWorkMode::TREE_MAP;
        break;

    default:
        break;
    }
    glutPostRedisplay();

}

/**
 *  GLUT reshape callback function
 **/
void ReshapeFunc(int width, int height)
{
    g_viewportWidth = width;
    g_viewportHeight = height;

    glViewport(0, 0, g_viewportWidth, g_viewportHeight);
    glutPostRedisplay();
}

/**
 * GLUT display callback function
 **/
void DisplayFunc()
{
    // projection setting
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, g_viewportWidth, g_viewportHeight, 0, 0, 1);

    //    glTranslated(5, 5, 0);

    // model-view setting
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    RenderGLScene();
    // swap buffers
    glutSwapBuffers();

}

/**
 *  GLUT keyboard callback function
 **/
void KeyboardFunc(unsigned char ch, int x, int y)
{
    switch (ch) {
    case 27: // ESC
        exit(0);
        break;
    case 8: // Backspace
    case 127: // DELETE
        if (g_working_mode == EnumWorkMode::TREE_MAP) {
            g_treemap->undo();
            g_treemap->reconstruct();
        }
        else{

             g_graph->undo();
             g_graph->reconstruct();}

        break;
    case 'm': 
              if (g_working_mode == EnumWorkMode::HIER_GRAPH)
               g_graph->constructMain();
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
        if (g_working_mode == EnumWorkMode::TREE_MAP) {
            g_treemap->setLevelToExpand(ch-'0');
            g_treemap->reconstruct();
        }
        else{
            g_graph->setLevelToExpand(ch-'0');
            g_graph->reconstruct();
           }
        break;
    }
    glutPostRedisplay();
}

/**
 * GLUT mouse callback function
 **/
void MouseFunc(int button, int state, int x, int y)
{
    bool leftDown = (button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN);
    bool rightDown = (button == GLUT_RIGHT_BUTTON) && (state == GLUT_DOWN);

    g_mouseState.setLeftDown(leftDown);
    g_mouseState.setRightDown(rightDown);
    g_mouseState.setPosition(x, y);
    g_mouseState.setTimestamp(clock_now());

    g_mouseAction.update(g_mouseState);

    cout << g_mouseAction << endl;


    printf("(%d, %d)\n", x, y);
    bool sceneChange = PickOnMouseChange(g_mouseAction);

    if (sceneChange) {
        glutPostRedisplay();
    }
}

/**
 *  GLUT mouse motion callback function
 **/
void MotionFunc(int x, int y)
{
    printf("(%d, %d)\n", x, y);
    g_mouseState.setPosition(x, y);
    g_mouseState.setTimestamp(clock_now());
    g_mouseAction.update(g_mouseState);

    cout << g_mouseAction << endl;

    bool sceneChange = PickOnMouseChange(g_mouseAction);

    if (sceneChange) {
        glutPostRedisplay();
    }
}

void PassiveMotionFunc(int x, int y)
{

    printf("(%d, %d)\n", x, y);
    g_mouseState.setPosition(x, y);
    g_mouseState.setTimestamp(clock_now());

    g_mouseAction.update(g_mouseState);

    cout << g_mouseAction << endl;

    bool sceneChange = PickOnMouseChange(g_mouseAction);

    if (sceneChange) {
        glutPostRedisplay();
    }
}

/**
 * all the rendering work here
 *
 **/
void RenderGLScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glPushMatrix();
    glColor3f(0.0, 0.0, 1.0);
    //    draw_utility::drawLabel(vec2(400,400), "Hello, world");
    switch (g_working_mode) {
    case EnumWorkMode::HIER_GRAPH:
        g_graph->Render();
        break;
    case EnumWorkMode::TREE_MAP:
        g_treemap->Render();
        break;
    }

    glPopMatrix();
}

void DrawPickScenes()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    glPushMatrix();

    switch (g_working_mode) {
    case EnumWorkMode::HIER_GRAPH:
        g_graph->Pick();
        break;
    case EnumWorkMode::TREE_MAP:
        g_treemap->Pick();
        break;
    }

    glPopMatrix();
}

/**
 *  picking when the cursor move
 **/
bool PickOnMouseChange(const MouseAction& mouseAction)
{
    int newly_picked;
    if (readUidAt(mouseAction.x(), mouseAction.y(), newly_picked) == false)
        return false;

    bool sceneChange = false;
    switch (g_working_mode) {
    case EnumWorkMode::HIER_GRAPH:
            sceneChange = g_graph->onPick(newly_picked, mouseAction);
        break;
    case EnumWorkMode::TREE_MAP:
        sceneChange = g_treemap->onPick(newly_picked, mouseAction);
        break;
    }
    return sceneChange;
}

/**
 * 
 * @param x
 * @param y
 * @param uid
 * @return true if the pick is valid
 */
bool readUidAt(int x, int y, int& uid)
{
    // boundary check
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    auto cursorInViewport = [&viewport](int x, int y)
    {
        return (x >= viewport[0] && y >= viewport[1] &&
                x < viewport[0] + viewport[2] && y < viewport[1] + viewport[3]);
    };
    if (!cursorInViewport(x, y))
        return false;

    // disable features to avoid unexpected picks
    glDisable(GL_DITHER);
    DrawPickScenes();
    // get the pixel
    unsigned char pixel[3];
    glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
    // enable feature
    glEnable(GL_DITHER);

    uid = utility::color2id(pixel);
    printf("picked uid = %d\n", uid);

    return true;
}


using namespace std;

int main(int argc, char **argv)
{
<<<<<<< HEAD
    std::string font_path = "Consolas.ttf";
    FontManagerSingleton::getInstance()->setFont(font_path.c_str(), 14);
    std::string path = "ascii2.dot";
=======
    std::string font_path = "/Users/robin/workplace/cpp/comp5111_proj/callgraph_visualizer/Consolas.ttf";
    FontManagerSingleton::getInstance()->setFont(font_path.c_str(), 14);
    std::string path = "/Users/robin/workplace/cpp/comp5111_proj/ascii2.dot";
>>>>>>> ee353df51a4a1839940b96dcb7cc7892bae81eba
    g_graph = std::move(std::unique_ptr<MyGraph>(new MyGraph(path)));

    printf("computing layout\n");
<<<<<<< HEAD
//    g_graph->getForceDirectedLayout(640, 480);
//    g_graph->getHierarchicalLayout(640, 480);
    
    
    path = "gawk2.dot";
=======
    g_graph->getHierarchicalLayout(1000, 600);
    
    
    path = "/Users/robin/workplace/cpp/comp5111_proj/gawk2.dot";
>>>>>>> ee353df51a4a1839940b96dcb7cc7892bae81eba
    g_treemap = std::move(std::unique_ptr<TreeMapController>(new TreeMapController(path)));

    glutInit(&argc, argv);
    InitGL();
    InitMenu();
    glutMainLoop();
}

