/* 
 * File:   MouseState.h
 * Author: robin
 *
 * Created on March 28, 2014, 8:45 PM
 */

#ifndef MOUSESTATE_H
#define	MOUSESTATE_H

#include <chrono>
#include <ratio>
#include <ctime>
#include <functional>


#include "rect.h"

typedef std::chrono::steady_clock::time_point timestamp_t;
typedef std::chrono::duration<int, std::milli> milliseconds_t;

inline timestamp_t clock_now()
{
    return std::chrono::steady_clock::now();
}

class MouseState
{
    bool leftBtnPressed;
    bool rightBtnPressed;
    timestamp_t timestamp;
    int _x, _y;

public:
    MouseState();
    MouseState(int x, int y, bool lbPressed = false, bool rbPressed = false);
    MouseState(int x, int y, bool lbPressed, bool rbPressed, const timestamp_t& t);
    MouseState(const MouseState& orig);
    virtual ~MouseState();

    void setPosition(int x, int y);
    int x() const;
    int y() const;
    void setLeftDown(bool b);
    void setRightDown(bool b);
    bool isLeftDown() const;
    bool isRightDown() const;
    void setTimestamp(const timestamp_t& t);
    timestamp_t getTimestamp() const;

    friend std::ostream& operator<<(std::ostream& o, MouseState const& state);
};

std::ostream& operator <<(std::ostream& o, MouseState const& state);

enum class MouseActionType
{
    MOVE,
    LEFT_CLICK,
    RIGHT_CLICK,
    DRAG,
    NONE
};

class MouseAction
{
    MouseState btnChngEvt;
    MouseState transientPast;
    MouseActionType _action;

public:
    MouseAction();

    int x() const;
    int y() const;
    int oldX() const;
    int oldY() const;

    /**
     * 
     * @return Current mouse action type.
     */
    MouseActionType action() const;
    
    /**
     * Update the mouse action by passing in the current MouseState
     * @param state 
     */
    void update(const MouseState& state);
    
    
    friend std::ostream& operator<<(std::ostream& o, MouseAction const& act);

    //protected:
    //    double millisecondsToOldTimestamp(const MouseState& state);
};

std::ostream& operator<<(std::ostream& o, MouseAction const& act);


#endif	/* MOUSESTATE_H */

