/* 
 * File:   rect.cpp
 * Author: robin
 * 
 * Created on April 13, 2014, 1:48 PM
 */

#include "rect.h"
#include "Graph.h"

#include <cassert>

rect::rect()
{

}

rect::rect(const rect& orig)
{
    this->topLeft = orig.topLeft;
    this->bottomRight = orig.bottomRight;
}

rect::rect(double x1, double y1, double x2, double y2)
: topLeft(x1, y1), bottomRight(x2, y2)
{

    assert(x1 <= x2);
    assert(y1 <= y2);
}

rect::~rect()
{
}

double rect::area() const
{
    return (bottomRight[0] - topLeft[0])*(bottomRight[1] - topLeft[1]);
}

double rect::height() const
{
    return fabs(topLeft[1] - bottomRight[1]);
}

double rect::width() const
{
    return fabs(topLeft[0] - bottomRight[0]);
}

rect rect::cutAtHeight(double portion1, double portion2) const
{
    assert(portion1 <= portion2);
    assert(portion1 >= 0.0 && portion1 <= 1.0);
    assert(portion2 >= 0.0 && portion2 <= 1.0);

    double x1 = topLeft[0], x2 = bottomRight[0];
    double y1 = topLeft[1] + portion1 * (bottomRight[1] - topLeft[1]);
    double y2 = topLeft[1] + portion2 * (bottomRight[1] - topLeft[1]);

    assert(x1 <= x2);
    assert(y1 <= y2);
    return rect(x1, y1, x2, y2);
}

rect rect::cutAtWidth(double portion1, double portion2) const
{
    assert(portion1 <= portion2);
    assert(portion1 >= 0.0 && portion1 <= 1.0);
    assert(portion2 >= 0.0 && portion2 <= 1.0);

    double y1 = topLeft[1], y2 = bottomRight[1];
    double x1 = topLeft[0] + portion1 * (bottomRight[0] - topLeft[0]);
    double x2 = topLeft[0] + portion2 * (bottomRight[0] - topLeft[0]);

    assert(x1 <= x2);
    assert(y1 <= y2);
    return rect(x1, y1, x2, y2);
}

vec2 rect::getTopLeft() const
{
    return topLeft;
}

vec2 rect::getBottomRight() const
{
    return bottomRight;
}

rect rect::shrinkBy(double difLf, double difRt, double difTop, double difBtm) const
{
    double x1 = topLeft[0] + difLf;
    double x2 = bottomRight[0] - difRt;
    double y1 = topLeft[1] + difTop;
    double y2 = bottomRight[1] - difBtm;
    
    assert(x1 <= x2);
    assert(y1 <= y2);
    return rect(x1, y1, x2, y2);
}

std::ostream& operator<<(std::ostream& o, rect const& r)
{
    o << "[" << r.topLeft[0] << ", " << r.bottomRight[0] << "]";
    o << "x";
    o << "[" << r.topLeft[1] << ", " << r.bottomRight[1] << "]";
    return o;
}


