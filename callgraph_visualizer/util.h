/* 
 * File:   util.h
 * Author: robin
 *
 * Created on March 22, 2014, 2:21 PM
 */

#ifndef UTIL_H
#define	UTIL_H

#ifdef __APPLE__
#include "TargetConditionals.h"
#ifdef TARGET_OS_MAC
#include <GLUT/glut.h>
#include <OpenGL/OpenGL.h>
#endif
#elif defined _WIN32 || defined _WIN64
#include <windows.h>
#include <GL/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <iostream>
#include <unordered_map>

#include "graph_type.h"


namespace utility
{

    /**
     * convert id to color vector
     * @param id    [in] 
     * @param c     [out] 3-byte vector of color
     */
    inline void id2color(unsigned int id, GLubyte c[])
    {
        c[0] = id & 0xff;
        c[1] = (id >> 8) & 0xff;
        c[2] = (id >> 16) & 0xff;
    }

    /**
     * convert color vector to id
     * @param c [in] color vector
     * @return id
     */
    inline int color2id(GLubyte c[])
    {
        unsigned int ans = 0;
        ans = c[2];
        ans = (ans << 8) | c[1];
        ans = (ans << 8) | c[0];
        return int(ans);
    }

    /**
     * 
     * @param path  [in] The path to the file describing a DiGraph
     * @param g     [out] A DiGraph
     * @param dp    [out] Dynamic properties to store fields associated with g
     * @param uid2vertex    [out] A map from uid to vertex_descriptor
     */
    void loadDiGraph(const std::string& path,
                     DiGraph& g,
                     boost::dynamic_properties& dp,
                     std::unordered_map<int, DiGraph::vertex_descriptor>& uid2vertex);

    /**
     * 
     * @return the node with zero in-degree
     */
    int getRoot(const DiGraph& g);

};



#endif	/* UTIL_H */

