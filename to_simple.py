#!/Users/robin/anaconda/bin/python

import pygraphviz as pgv
import networkx as nx
import os
import sys

infile = sys.argv[1]
outfile = sys.argv[2]

G=pgv.AGraph(infile)
H=pgv.AGraph(strict=True, directed=True)

it=G.iternodes()
try:
    while True:
        n=it.next()
        H.add_node(n, label=str(n.attr['label']))
except StopIteration:
    pass

H.add_edges_from(set(G.edges()), weight=0)    
it=G.iteredges()
try:
    while True:
        e=it.next()
        dest = H.get_edge(e[0],e[1])
        w = dest.attr['weight']
        w = int(w) + 1
        dest.attr['weight'] = str(w)
except StopIteration:
    pass

H.write(outfile)

# output *.gml file
# G=nx.DiGraph(nx.read_dot(outfile))
# outfile = outfile+".gml"
# nx.write_gml(G, outfile) 

